include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: all
	$(INSTALL) -D -p -m 0644 odl/mcastd_noforward.odl $(DEST)/etc/amx/tr181-mcastd/extensions/mcastd_noforward.odl
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mcastd_noforward.so $(DEST)/usr/lib/amx/mcastd-ext/mcastd_noforward.so
	$(INSTALL) -D -p -m 0755 scripts/S10noforward $(DEST)/etc/amx/tr181-mcastd/init.d/S10noforward
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/tr181-mcastd/debuginfo
	ln -sfr $(DEST)/etc/amx/tr181-mcastd/init.d/S10noforward $(DEST)/etc/amx/tr181-mcastd/debuginfo/S10noforward

package: all
	$(INSTALL) -D -p -m 0644 odl/mcastd_noforward.odl $(PKGDIR)/etc/amx/tr181-mcastd/extensions/mcastd_noforward.odl
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mcastd_noforward.so $(PKGDIR)/usr/lib/amx/mcastd-ext/mcastd_noforward.so
	$(INSTALL) -D -p -m 0755 scripts/S10noforward $(PKGDIR)/etc/amx/tr181-mcastd/init.d/S10noforward
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/tr181-mcastd/debuginfo
	rm -f $(PKGDIR)/etc/amx/tr181-mcastd/debuginfo/S10noforward
	ln -sfr $(PKGDIR)/etc/amx/tr181-mcastd/init.d/S10noforward $(PKGDIR)/etc/amx/tr181-mcastd/debuginfo/S10noforward
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/mcastd_noforward.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

.PHONY: all clean changelog install package doc