# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.4 - 2024-05-14(06:15:36 +0000)

### Other

- Add support for multicast with MLDv2 (IPv6)

## Release v0.1.3 - 2024-04-23(14:24:53 +0000)

### Other

- Port multicast proxy plugin (KMCD) to amx

## Release v0.1.2 - 2023-12-05(12:29:42 +0000)

### Other

- Add missing dependency on ebtables

## Release v0.1.1 - 2023-12-05(12:17:12 +0000)

### Other

- Rename mcastd plugin to tr181-mcastd

## Release v0.1.0 - 2023-12-05(11:54:14 +0000)

### New

- port multicast proxy plugin to amx

### Other

- Opensource component

