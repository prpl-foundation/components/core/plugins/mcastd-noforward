/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>

#include <netmodel/client.h>

#include <mcastd/user.h>

#define STR_EMPTY(x) ((x) == NULL || (x)[0] == '\0')
#define ME "noforward"

typedef struct nofw_intf {
    amxd_object_t* object;
    char* netdev_name;
    bool ipv6;
    netmodel_query_t* drop_query;
    amxc_var_t drop_query_ports;
    netmodel_query_t* accept_query;
    amxc_var_t accept_query_ports;
} nofw_intf_t;

static int nofw_dummy;
static void* nofw_key = &nofw_dummy;
static bool s_stopping = false;

static inline void _variant_print(const amxc_var_t* var) {
    if(sahTraceLevel() >= TRACE_LEVEL_INFO) {
        amxc_var_log(var);
    }
}

/* Add rules that drop IGMP/MLDv1 packets on output interfaces */
static void nofw_setDropRules(nofw_intf_t* intf) {
    amxc_var_for_each(var, &intf->drop_query_ports) {
        const char* netdev_name = GET_CHAR(var, NULL);
        if(!STR_EMPTY(netdev_name)) {
            if(intf->ipv6) { /*MLDv1 reports*/
                mcastd_shprintf("ebtables -A FORWARD_mcastd -o %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 131 -j DROP", netdev_name);
                /*MLDv2 reports*/
                mcastd_shprintf("ebtables -A FORWARD_mcastd -o %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 143 -j DROP", netdev_name);
            } else {
                mcastd_shprintf("ebtables -A FORWARD_mcastd -o %s -p 0x0800 --ip-proto igmp -j DROP", netdev_name);
            }
        }
    }
}

/* Add the specific rules that accept IGMP/MLDv1 packets on the upstream interface */
/* These rules should come before any DROP rule => insert them with index 1 */
static void nofw_setAcceptRules(nofw_intf_t* intf) {
    amxc_var_for_each(var, &intf->accept_query_ports) {
        const char* netdev_name = GET_CHAR(var, NULL);
        if(!STR_EMPTY(netdev_name)) {
            if(intf->ipv6) { //MLDv1 reports
                mcastd_shprintf("ebtables -I FORWARD_mcastd 1 -i %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 131 -j ACCEPT", netdev_name);
                /*MLDv2 reports*/
                mcastd_shprintf("ebtables -I FORWARD_mcastd 1 -i %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 143 -j ACCEPT", netdev_name);
            } else {
                mcastd_shprintf("ebtables -I FORWARD_mcastd 1 -i %s -p 0x800 --ip-proto igmp -j ACCEPT", netdev_name);
            }
        }
    }
}

/* Clear rules that drop IGMP/MLDv1 packets on output interfaces */
static void nofw_clrDropRules(nofw_intf_t* intf) {
    amxc_var_for_each(var, &intf->drop_query_ports) {
        const char* netdev_name = GET_CHAR(var, NULL);
        if(!STR_EMPTY(netdev_name)) {
            if(intf->ipv6) { /*MLDv1 reports*/
                mcastd_shprintf("ebtables -D FORWARD_mcastd -o %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 131 -j DROP", netdev_name);
                /*MLDv2 reports*/
                mcastd_shprintf("ebtables -D FORWARD_mcastd -o %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 143 -j DROP", netdev_name);
            } else {
                mcastd_shprintf("ebtables -D FORWARD_mcastd -o %s -p 0x800 --ip-proto igmp -j DROP", netdev_name);
            }
        }
    }
}

/* Clear the specific rules that accept IGMP/MLDv1 packets on the upstream interface */
static void nofw_clrAcceptRules(nofw_intf_t* intf) {
    amxc_var_for_each(var, &intf->accept_query_ports) {
        const char* netdev_name = GET_CHAR(var, NULL);
        if(!STR_EMPTY(netdev_name)) {
            if(intf->ipv6) { /*MLDv1 reports*/
                mcastd_shprintf("ebtables -D FORWARD_mcastd -i %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 131 -j ACCEPT", netdev_name);
                /*MLDv2 reports*/
                mcastd_shprintf("ebtables -D FORWARD_mcastd -i %s -p 0x86DD "
                                "--ip6-protocol 58 --ip6-icmp-type 143 -j ACCEPT", netdev_name);
            } else {
                mcastd_shprintf("ebtables -D FORWARD_mcastd -i %s -p 0x800 --ip-proto igmp -j ACCEPT", netdev_name);
            }
        }
    }
}

static void nofw_portsDropHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    nofw_intf_t* intf = (nofw_intf_t*) userdata;

    SAH_TRACEZ_INFO(ME, "Enter, ipv6(%d)", intf->ipv6);
    nofw_clrDropRules(intf);

    if(amxc_var_type_of(result) != AMXC_VAR_ID_HTABLE) {
        return;
    }

    _variant_print(result);
    amxc_var_copy(&intf->drop_query_ports, result);

    nofw_setDropRules(intf);
    SAH_TRACEZ_INFO(ME, "Leave");
}

static void nofw_portsAcceptHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    nofw_intf_t* intf = (nofw_intf_t*) userdata;

    SAH_TRACEZ_INFO(ME, "Enter, ipv6(%d)", intf->ipv6);
    nofw_clrAcceptRules(intf);

    if(amxc_var_type_of(result) != AMXC_VAR_ID_HTABLE) {
        return;
    }

    _variant_print(result);
    amxc_var_copy(&intf->accept_query_ports, result);

    nofw_setAcceptRules(intf);
    SAH_TRACEZ_INFO(ME, "Leave");
}

static void nofw_openQueries(nofw_intf_t* intf) {
    const char* netmodel_intf = GET_CHAR(amxd_object_get_param_value(intf->object, "NetmodelIntf"), NULL); // can't use netdevname, must be netmodel intf
    intf->drop_query = netmodel_openQuery_getParameters(netmodel_intf,
                                                        "mcastd_nofw", "Name", "inbridge",
                                                        netmodel_traverse_down, nofw_portsDropHandler, intf);

    intf->accept_query = netmodel_openQuery_getParameters(netmodel_intf,
                                                          "mcastd_nofw", "NetDevName", "upstream && netdev-bound",
                                                          "one level down", nofw_portsAcceptHandler, intf);
}

static void nofw_closeQueries(nofw_intf_t* intf) {
    if(intf->drop_query != NULL) {
        netmodel_closeQuery(intf->drop_query);
        intf->drop_query = NULL;
    }
    if(intf->accept_query != NULL) {
        netmodel_closeQuery(intf->accept_query);
        intf->accept_query = NULL;
    }
    if(s_stopping) {
        /**
         * Do not clear the ebtables rules one by one from FORWARD_mcastd.
         * S10noforward will flush all rules from FORWARD_mcastd with one flush
         * command.
         */
        return;
    }
    nofw_clrDropRules(intf);
    nofw_clrAcceptRules(intf);
}

static void nofw_intf_put(nofw_intf_t* intf) {
    nofw_closeQueries(intf);
    free(intf->netdev_name);
    amxc_var_clean(&intf->drop_query_ports);
    amxc_var_clean(&intf->accept_query_ports);
    free(intf);
}

static nofw_intf_t* nofw_intf_get(const char* netdev_name, const char* family, amxd_object_t* object) {
    nofw_intf_t* intf;
    intf = calloc(1, sizeof(nofw_intf_t));
    intf->object = object;
    amxc_var_init(&intf->drop_query_ports);
    amxc_var_init(&intf->accept_query_ports);
    intf->netdev_name = strdup(netdev_name);
    if(!intf->netdev_name) {
        SAH_TRACEZ_ERROR(ME, "strdup() failed");
        goto error;
    }
    if(strcmp(family, "ipv4")) {
        intf->ipv6 = true;
    }
    nofw_openQueries(intf);
    return intf;
error:
    nofw_intf_put(intf);
    return NULL;
}

static void nofw_enable(amxd_object_t* object) {
    const char* netdev_name = GET_CHAR(amxd_object_get_param_value(object, "NetDevName"), NULL);
    const char* family = GET_CHAR(amxd_object_get_param_value(object, "Family"), NULL);
    nofw_intf_t* intf = mcastd_getObjectUserData(object, nofw_key);

    if(intf) {
        if(!strcmp(intf->netdev_name? :"", netdev_name? :"")) { // nop
            return;
        }
        mcastd_delObjectUserData(object, nofw_key);
        nofw_intf_put(intf);
    }
    if(netdev_name && *netdev_name) {
        intf = nofw_intf_get(netdev_name, family, object);
        if(!intf) {
            return;
        }
        mcastd_addObjectUserData(object, nofw_key, intf);
    }
}

static void nofw_disable(amxd_object_t* object) {
    nofw_intf_t* intf = mcastd_getObjectUserData(object, nofw_key);
    if(!intf) {
        return;
    }
    mcastd_delObjectUserData(object, nofw_key);
    nofw_intf_put(intf);
}

static void nofw_objectWriteHandler(amxd_object_t* object) {
    if(amxd_object_get_value(bool, object, "NoForwardEnable", NULL)) {
        nofw_enable(object);
    } else {
        nofw_disable(object);
    }
}

static void nofw_objectDelHandler(amxd_object_t* object) {
    nofw_disable(object);
}

__attribute__((constructor))
static void nofw_init(void) {
    mcastd_addObjectWriteHandler("MCASTD.Intf.*", nofw_objectWriteHandler);
    mcastd_addObjectDelHandler("MCASTD.Intf.*", nofw_objectDelHandler);
}

__attribute__((destructor))
static void nofw_cleanup(void) {
    amxd_object_t* intf = amxd_dm_findf(get_dm(), "MCASTD.Intf");
    s_stopping = true;
    mcastd_delObjectWriteHandler("MCASTD.Intf.*", nofw_objectWriteHandler);
    mcastd_delObjectDelHandler("MCASTD.Intf.*", nofw_objectDelHandler);
    amxd_object_for_each(instance, it, intf) {
        amxd_object_t* object = amxc_container_of(it, amxd_object_t, it);
        nofw_objectDelHandler(object);
    }
}

